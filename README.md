#Find the celebrity' project

## Requirements
* [OpenJdk 8](https://adoptopenjdk.net/)
* [Maven](https://maven.apache.org/install.html)

## Description

This project use a Collection of People to find which of this could be a celebrity. The class in charge is the Celebrity
which make different validation like create a relationship between each person contain
in the list and find the celebrity according the following requirements: 
* Everyone knows about the celebrity, but the celebrity doesn't know anything about the rest of the people.

## Test
This project contains the CelebrityTest class which is used to validate the different scenarios when you can find a celebrity between a regular group of people.

## Running locally
Use the default IDE configuration to run the Junit Test Coverage