import co.com.celebrity.Celebrity;
import co.com.celebrity.Person;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author noel.gonzalez
 */
public class CelebrityTest {

  public static final String THE_ROCK_WAYNE_JOHNSON = "The Rock! (Wayne johnson).";
  public static final String REGULAR_PERSON = "Regular person";
  public static final String ROBERT_DOWNEY_JR = "Robert Downey, Jr.";

  @Before
  public void init() {
  }

  @Test
  public void validateCelebrity() {
    Celebrity celebrity = new Celebrity(getCelebrityBetweenRegularPeople());
    Assert.assertEquals(celebrity.findCelebrity().get().getName(), THE_ROCK_WAYNE_JOHNSON);
  }

  @Test
  public void validateCelebrityIronMan() {
    Celebrity celebrity = new Celebrity(getIronManBetweenRegularPeople());
    Assert.assertEquals(celebrity.findCelebrity().get().getName(), ROBERT_DOWNEY_JR);
  }

  @Test(expected=java.util.NoSuchElementException.class)
  public void validateNonCelebrity() {
    Celebrity celebrity = new Celebrity(getRegularPeople());
    Assert.assertNull(celebrity.findCelebrity().get().getName(), REGULAR_PERSON);
  }

  @Test(expected=java.util.NoSuchElementException.class)
  public void validateNonCelebrityInRegularGroup() {
    Celebrity celebrity = new Celebrity(getJustAnotherRegularPersonTest());
    Assert.assertNull(celebrity.findCelebrity().get().getName(), REGULAR_PERSON);
  }


  private List<Person> getRegularPeople() {

    Person celebrity = new Person(2, REGULAR_PERSON);
    Person person1 = new Person(0, REGULAR_PERSON);
    Person person2 = new Person(1, REGULAR_PERSON);
    Person person3 = new Person(3, REGULAR_PERSON);

    celebrity.setKnownPersons(List.of(person3));
    person1.setKnownPersons(List.of(celebrity));
    person2.setKnownPersons(List.of(celebrity));
    person3.setKnownPersons(List.of(celebrity));

    return List.of(celebrity,person1,person2,person3);
  }


  private List<Person> getCelebrityBetweenRegularPeople() {
    Person celebrity = new Person(2, THE_ROCK_WAYNE_JOHNSON);
    Person person1 = new Person(0, REGULAR_PERSON);
    Person person2 = new Person(1, REGULAR_PERSON);
    Person person3 = new Person(3, REGULAR_PERSON);

    person1.setKnownPersons(List.of(celebrity));
    person2.setKnownPersons(List.of(celebrity));
    person3.setKnownPersons(List.of(celebrity));

    return List.of(celebrity,person1,person2,person3);
  }


  private List<Person> getIronManBetweenRegularPeople() {
    Person celebrity = new Person(3, ROBERT_DOWNEY_JR);

    Person person1 = new Person(0, REGULAR_PERSON);
    Person person2 = new Person(1, REGULAR_PERSON);
    Person person3 = new Person(2, REGULAR_PERSON);
    Person person4 = new Person(4, REGULAR_PERSON);

    person1.setKnownPersons(List.of(person2, celebrity));
    person2.setKnownPersons(List.of(celebrity));
    person3.setKnownPersons(List.of(celebrity));
    person4.setKnownPersons(List.of(celebrity));

  return List.of(celebrity,person1,person2,person3, person4);
  }

  private List<Person> getJustAnotherRegularPersonTest() {
    Person regular = new Person(3, REGULAR_PERSON);

    Person person1 = new Person(0, REGULAR_PERSON);
    Person person2 = new Person(1, REGULAR_PERSON);
    Person person3 = new Person(2, REGULAR_PERSON);
    Person person4 = new Person(4, REGULAR_PERSON);

    person1.setKnownPersons(List.of(person2, regular));
    person2.setKnownPersons(List.of(regular));
    person3.setKnownPersons(List.of(regular));
    person4.setKnownPersons(List.of(regular));
    regular.setKnownPersons(List.of(regular));

    return List.of(regular,person1,person2,person3, person4);
  }

}
