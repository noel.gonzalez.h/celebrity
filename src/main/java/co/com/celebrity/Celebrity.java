package co.com.celebrity;

import java.util.List;
import java.util.Optional;

/**
 * @author noel.gonzalez
 */
public class Celebrity {

  List<Person> people;
  private int[][] relationship;

  public Celebrity(List<Person> people) {
    this.people = people;
    createRelationShip();
  }

  public Optional<Person> findCelebrity() {
    int possibleCelebrity = -1;
    for (int j = 0; j < people.size(); j++) {
      for (int i = 0; i < people.size(); i++) {
        if (relationship[j][i] == 1 && j != i) {
          possibleCelebrity = i;
        }
      }
    }

    for (int i = 0; i < people.size(); i++) {
      if (relationship[possibleCelebrity][i] != 0) {
        possibleCelebrity = -1;
        break;
      }
    }

    int finalPossibleCelebrity = possibleCelebrity;
    Optional<Person> celebrity = people.stream()
        .filter(person -> person.getId() == finalPossibleCelebrity)
        .findFirst();

    printRelationship();

    if (!celebrity.isPresent()) {
      System.out.println("Celebrity not found.!");
    } else {
      System.out.println("The Celebrity is: " + celebrity.get().getName());
    }

    return celebrity;
  }

  private void createRelationShip() {
    relationship = new int[people.size()][people.size()];
    for (Person person : people) {
      List<Person> relation = person.getKnownPersons();
      if (null != relation && !relation.isEmpty()) {
        for (Person knowPerson : relation) {
          relationship[person.getId()][knowPerson.getId()] = 1;
        }
      }
    }
  }

  private void printRelationship() {
    System.out.println("People size: " + people.size());
    for (int i = 0; i < people.size(); i++) {
      for (int j = 0; j < people.size(); j++) {
        if (j == people.size() - 1) {
          System.out.println(relationship[i][j]);
        } else {
          System.out.print(relationship[i][j]);
        }
      }
    }
  }

}
