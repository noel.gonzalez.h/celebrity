package co.com.celebrity;

import java.util.List;

public class Person {

  private Integer id;
  private String name;
  private List<Person> knownPersons;

  public Person(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(obj);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public List<Person> getKnownPersons() {
    return knownPersons;
  }

  public void setKnownPersons(List<Person> knownPersons) {
    this.knownPersons = knownPersons;
  }
}
