import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author noel.gonzalez
 */
public class Test {

  public static final String STRING = "\\.";
  public static final char DELIMITER = '.';

  public static void main(String[] args) {

    /**
     * You must write a method in such a way that it can be used to validate an IP address. Use the following definition of an IP address:
     *
     * IP address  a string in the form "A.B.C.D", where the value of A, B, C, and D may range from 0 to 255. Leading zeros are allowed.
     *
     * Some valid IP address:
     * 000.12.12.034
     * 121.234.12.12
     * 23.45.12.56
     *
     * Some invalid IP address:
     * 000.12.234.23.23
     * 666.666.23.23
     * .213.123.23.32
     * 23.45.22.32.
     * I.Am.not.an.ip
     *
     * Sample Input
     * 000.12.12.034
     * 121.234.12.12
     * 23.45.12.56
     * 00.12.123.123123.123
     * 122.23
     * Hello.IP
     *
     * Sample Output
     * true
     * true
     * true
     * false
     * false
     * false
     *
     */

    System.out.println(" Dir IP .000.12.12.034 valid " + validateIP(".000.12.12.034"));
    System.out.println(" Dir IP 000.12.12.000 valid " + validateIP("000.12.12.000"));
    System.out.println(" Dir IP 000.12.12.034 valid " + validateIP("000.12.12.034"));
    System.out.println(" Dir IP 121.234.12.12 valid " + validateIP("121.234.12.12"));
    System.out.println(" Dir IP 23.45.12.56 valid " + validateIP("23.45.12.56"));
    System.out.println(" Dir IP 000.12.234.23.23 valid " + validateIP("000.12.234.23.23"));
    System.out.println(" Dir IP 666.666.23.23 valid " + validateIP("666.666.23.23"));
    System.out.println(" Dir IP .213.123.23.32 valid " + validateIP(".213.123.23.32"));
    System.out.println(" Dir IP 23.45.22.32. valid " + validateIP("23.45.22.32."));
    System.out.println(" Dir IP I.Am.not.an.ip valid " + validateIP("I.Am.not.an.ip"));
    System.out.println(" Dir IP 000.12.12.034 valid " + validateIP("000.12.12.034"));
    System.out.println(" Dir IP 121.234.12.12 valid " + validateIP("121.234.12.12"));
    System.out.println(" Dir IP 23.45.12.56 valid " + validateIP("23.45.12.56"));
    System.out.println(" Dir IP 00.12.123.123123.123  valid " + validateIP("00.12.123.123123.123"));
    System.out.println(" Dir IP 122.23 valid " + validateIP("122.23"));
    System.out.println(" Dir IP Hello.IP valid " + validateIP("Hello.IP"));

  }

  private static boolean validateIP(String dir) {
    if (!isValid(dir)) {
      return false;
    }
    List<String> values;
    try {
      values = Arrays.stream(dir.split(STRING))
          .filter(current -> Integer.parseInt(current) < 0 || Integer.parseInt(current) > 255)
          .collect(Collectors.toList());
    } catch (Exception e) {
      return false;
    }
    return values.isEmpty();
  }

  private static boolean isValid(String dir) {
   return dir.charAt(0) != DELIMITER && dir.charAt(dir.length() - 1) != DELIMITER && dir.split(STRING).length == 4;
  }

}
